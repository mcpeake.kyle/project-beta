# CarCar
This is an application for an automobile dealership with an inventory, service center and sales.

Docker is used to run containers for the application.
Django is used to create RESTful APIs in microservices for the back-end.
React is used to interacts with the RESTful APIs to create the front-end.


Team:

* Kyle McPeake - Service
* Harry Huynh - Sales (INCOMPLETE)
##
# How to Run this App
**A note for macOS users**:
*Typing one of the following commands,* `docker-compose up`, *will give a warning about a missing environment variable name "OS". This can be ignored.*

1. Fork this repository, and clone it to your computer.
2. Install Docker Desktop if you do not have it: "https://docs.docker.com/get-docker/"
3. Open Docker Desktop and keep it running.
4. In your terminal, type the following commands:

    `docker volume create beta-data`

    `docker-compose build`

    `docker-compose up`

5. View the project in your browser by going to "http://localhost:3000/"
##
# Diagram
![alt text](Diagram.jpg)

##
# Inventory API
Inventory is where manufacturers, models, and automobile information is created, and a list of each can be viewed.

ports 8100:8000

URLs
- http://localhost:3000/listManufacturers
- http://localhost:3000/createManufacturer
- http://localhost:3000/listVehicleModels
- http://localhost:3000/createModel
- http://localhost:3000/listAutomobiles
- http://localhost:3000/createAutomobile

### Manufacturers
Action / Method / URL
- List manufacturers / GET / http://localhost:8100/api/manufacturers/
- Create a manufacturer / POST / http://localhost:8100/api/manufacturers/
- Get a specific manufacturer / GET / http://localhost:8100/api/manufacturers/:id/
- Update a specific manufacturer / PUT / http://localhost:8100/api/manufacturers/:id/
- Delete a specific manufacturer / DELETE / http://localhost:8100/api/manufacturers/:id/

### Vehicle Models
Action / Method / URL
- List vehicle models / GET / http://localhost:8100/api/models/
- Create a vehicle model / POST / http://localhost:8100/api/models/
- Get a specific vehicle model / GET / http://localhost:8100/api/models/:id/
- Update a specific vehicle model / PUT / http://localhost:8100/api/models/:id/
- Delete a specific vehicle model / DELETE / http://localhost:8100/api/models/:id/

### Automobile Information
Action / Method / URL
- List automobiles / GET / http://localhost:8100/api/automobiles/
- Create an automobile / POST / http://localhost:8100/api/automobiles/
- Get a specific automobile / GET / http://localhost:8100/api/automobiles/:vin/
- Update a specific automobile / PUT / http://localhost:8100/api/automobiles/:vin/
- Delete a specific automobile / DELETE / http://localhost:8100/api/automobiles/:vin/
##
# Service API
Service is where technicians and appointment information is created, and a list of each can be viewed. If a vehicle's VIN exists within the Inventory data when the appointment is created, that means it was sold from CarCar and the appointment is given VIP status. The list of service appointments shows all "created" appointments and has an option to either "cancel" or "finish" an appointment. When an option is selected, the appointment is removed from the list but it is not deleted. The appointment can still be viewed under "service history" along with the updated status. All appointments can be viewed in "service history". There is an option to filter (search) "service history" for all appointments by VIN.

ports 8080:8000

URLs
- http://localhost:3000/listTechnician
- http://localhost:3000/addTechnician
- http://localhost:3000/serviceAppointmentList
- http://localhost:3000/serviceAppointmentCreate
- http://localhost:3000/serviceHistory
### Technicians
Action / Method / URL
- List technicians / GET / http://localhost:8080/api/technicians/
- Create a technician / POST / http://localhost:8080/api/technicians/
- Delete a specific technician / DELETE / http://localhost:8080/api/technicians/:id/
### Appointments
Action / Method / URL
- List appointments / GET / http://localhost:8080/api/appointments/
- Create an appointment / POST / http://localhost:8080/api/appointments/
- Delete an appointment / DELETE / http://localhost:8080/api/appointments/:id/
- Set appointment status to "canceled" / PUT / http://localhost:8080/api/appointments/:id/cancel/
- Set appointment status to "finished" / PUT / http://localhost:8080/api/appointments/:id/finish/
##
