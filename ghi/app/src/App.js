import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';

import ListManufacturers from './Inventory/ListManufacturers';
import CreateManufacturer from './Inventory/CreateManufacturer';
import ListVehicleModels from './Inventory/ListVehicleModels';
import CreateModel from './Inventory/CreateModel';
import ListAutomobiles from './Inventory/ListAutomobiles';
import CreateAutomobile from './Inventory/CreateAutomobile';

// import SalepersonForm from './Sales/SalesPersonForm';
// import SalespersonList from './Sales/SalesPersonList';
// import CustomerList from './Sales/CustomerList';
// import CustomerForm from './Sales/CustomersForm';

import ListTechnician from './Service/ListTechnicians';
import AddTechnician from './Service/AddTechnician';
import ServiceAppointmentList from './Service/ServiceAppointmentList';
import ServiceAppointmentCreate from './Service/ServiceAppointmentCreate';
import ServiceHistory from './Service/ServiceHistory';



export default function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route path="/listManufacturers" element={<ListManufacturers />} />
          <Route path="/createManufacturer" element={<CreateManufacturer />} />
          <Route path="/listVehicleModels" element={<ListVehicleModels />} />
          <Route path="/createModel" element={<CreateModel />} />
          <Route path="/listAutomobiles" element={<ListAutomobiles />} />
          <Route path="/createAutomobile" element={<CreateAutomobile />} />

          {/* <Route path='/salespeople/new' element={<SalepersonForm />} />
          <Route path='salespeople' element={<SalespersonList />} />
          <Route path='customers' element={<CustomerList />} />
          <Route path='/customers/new' element={<CustomerForm />} />
          <Route path='/sales' />
          <Route path='/sales/new' />
          <Route path='/salesHistory' /> */}

          <Route path="/listTechnician" element={<ListTechnician/>}/>
          <Route path="/addTechnician" element={<AddTechnician/>}/>
          <Route path="/serviceAppointmentList" element={<ServiceAppointmentList/>}/>
          <Route path="/serviceAppointmentCreate" element={<ServiceAppointmentCreate/>}/>
          <Route path="/serviceHistory" element={<ServiceHistory/>}/>

        </Routes>
      </div>
    </BrowserRouter>
  );
}
