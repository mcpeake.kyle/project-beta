import React, { useState, useEffect } from 'react'

export default function NewManufacturer() {
    const [formData, setFormData] = useState({
        color: '',
        year: '',
        vin: '',
        model_id: '',
    })

    const handleInputChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const url = 'http://localhost:8100/api/automobiles/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setFormData({
                color: '',
                year: '',
                vin: '',
                model_id: '',
            })
        }
    }

    const [model, setModel] = useState([])

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setModel(data.models)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <main>
            <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create an automobile to inventory</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                    <div className="form-floating mb-3">
                        <input required type="text" name="color" id="color" className="form-control"
                        value={formData.color} onChange={handleInputChange} />
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input required type="number" name="year" id="year" className="form-control"
                        value={formData.year} onChange={handleInputChange} />
                        <label htmlFor="year">Year</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input required type="text" name="vin" id="vin" className="form-control"
                        value={formData.vin} onChange={handleInputChange} />
                        <label htmlFor="vin">VIN</label>
                    </div>
                    <div className="mb-3">
                        <select required name="model_id" id="model_id" className="form-select" value={formData.model_id} onChange={handleInputChange}>
                        <option value="">Choose a model</option>
                        {model.map(model => {
                            return (
                                <option key={model.id} value={model.id}>{model.name}</option>
                            )
                        })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>
            </div>
        </main>
            );
}
