import React, { useState, useEffect } from 'react'

export default function NewManufacturer() {
    const [formData, setFormData] = useState({
        name: '',
        picture_url: '',
        manufacturer_id: '',
    })

    const handleInputChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const url = 'http://localhost:8100/api/models/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setFormData({
                name: '',
                picture_url: '',
                manufacturer_id: '',
            })
        }
    }

    const [manufacturer, setManufacturer] = useState([])

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setManufacturer(data.manufacturers)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <main>
            <div className="container">
            <div className="row">
                <div className="shadow p-4 mt-4">
                    <h1>Create a vehicle model</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                    <div className="form-floating mb-3">
                        <input required type="text" name="name" id="name" className="form-control"
                        value={formData.name} onChange={handleInputChange} />
                        <label htmlFor="name">Model name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input required type="text" name="picture_url" id="picture_url" className="form-control"
                        value={formData.picture_url} onChange={handleInputChange} />
                        <label htmlFor="picture_url">Picture URL</label>
                    </div>
                    <div className="mb-3">
                        <select required name="manufacturer_id" id="manufacturer_id" className="form-select" value={formData.manufacturer_id} onChange={handleInputChange}>
                        <option value="">Choose a Manufacturer</option>
                        {manufacturer.map(manufacturer => {
                            return (
                                <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                            )
                        })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>

                    </form>
                </div>
                </div>
            </div>
        </main>
    );
}
