import React, {useEffect, useState } from 'react';

export default function ListAutomobiles(props) {
  const [automobiles, setAutomobiles] = useState([])

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/automobiles/';
    const response = await fetch(url);
    if (response.ok) {
        const automobiles = await response.json();
        setAutomobiles(automobiles.autos)

    }
}

    useEffect(() => {
      fetchData()
    }, [])

    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
            <th>Sold</th>
          </tr>
        </thead>
        <tbody>
          {automobiles?.map(automobiles => {
            return (
              <tr key={automobiles.id}>
                <td>{ automobiles.vin }</td>
                <td>{ automobiles.color }</td>
                <td>{ automobiles.year }</td>
                <td>{ automobiles.model.name }</td>
                <td>{ automobiles.model.manufacturer.name }</td>
                <td>{ automobiles.sold }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
