import React, {useEffect, useState } from 'react';

export default function ManufacturersList(props) {
  const [manufacturers, setManufacturers] = useState([])

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/manufacturers/';
    const response = await fetch(url);
    if (response.ok) {
        const {manufacturers} = await response.json();
        setManufacturers(manufacturers)
    }
}

    useEffect(() => {
      fetchData()
    }, [])

    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers.map(manufacturers => {
            return (
              <tr key={manufacturers.href}>
                <td>{ manufacturers.name }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
