import React, {useEffect, useState } from 'react';

export default function ListVehicleModels(props) {
  const [models, setModels] = useState([])

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/models/';
    const response = await fetch(url);
    if (response.ok) {
        const {models} = await response.json();
        setModels(models)
    }
}

    useEffect(() => {
      fetchData()
    }, [])

    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {models?.map(models => {
            return (
              <tr key={models.id}>
                <td>{ models.name }</td>
                <td>{ models.manufacturer.name }</td>
                <td><img src={models.picture_url} width={150} height={100} /></td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
