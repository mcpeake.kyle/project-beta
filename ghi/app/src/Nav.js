import { NavLink } from 'react-router-dom';

export default function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" to="/createManufacturer">Create Manufacturer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/listManufacturers">List Manufacturers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/createModel">Create Model</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/listVehicleModels">List Models</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/createAutomobile">Create Automobile</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/listAutomobiles">List Automobiles</NavLink>
            </li>
          </ul>
        </div>
        {/* <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className='"nav-item'><NavLink className="nav-link active" to='/salespeople'>Salespeople</NavLink></li>
            <li className='nav-item'><NavLink className="nav-link active" to='/salespeople/new'>Add a Saleperson</NavLink></li>
            <li className='nav-item'><NavLink className="nav-link active" to='/customers'>Customers</NavLink></li>
            <li className='nav-item'><NavLink className='nav-link active' to='/customers/new'>Add a Customer</NavLink></li>
            <li className='nav-item'><NavLink className='nav-link active' to='/sales'>Sales</NavLink></li>
            <li className='nav-item'><NavLink className='nav-link active' to='/sales/new'>Add a Sale</NavLink></li>
            <li className='nav-item'><NavLink className='nav-link active' to='/salesHistory'>Sales History</NavLink></li>
          </ul>
        </div> */}
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" to="/addTechnician">Add Technician</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/listTechnician">List Technicians</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/serviceAppointmentCreate">Create Service Appointment</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/serviceAppointmentList">List Service Appointments</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/serviceHistory">Service History</NavLink>
            </li>
          </ul>
        </div>

      </div>
    </nav>
  )
}
