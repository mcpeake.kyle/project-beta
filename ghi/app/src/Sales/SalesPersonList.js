import { useEffect, useState } from "react";

function SalespersonList() {
    const [ salespeople, setSalespeople] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        }
    }
    
    const deleteIt = async(id) => {
        const del = `http://localhost:8090/api/salespeople/${id}/`;
        const fetchConfig = {method: 'delete'};
        const response = await fetch(del, fetchConfig);
        if (response.ok) {
            fetchData();
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>FIrst Name</th>
                    <th>Last Name</th>
                    <th>Employee ID</th>
                </tr>
            </thead>
            <tbody>
                {salespeople.map(person => {
                    return (
                        <tr key={person.id}>
                            <td>{ person.first_name }</td>
                            <td>{ person.last_name }</td>
                            <td>{ person.employee_id }</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>

    )
}
export default SalespersonList;
