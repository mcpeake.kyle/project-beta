import React, { useState } from 'react'

export default function NewLocation() {
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        employee_id: '',
    })

    const handleInputChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const url = 'http://localhost:8080/api/technicians/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setFormData({
                first_name: '',
                last_name: '',
                employee_id: '',
            })
        }
    }
    return (
        <main>
            <div className="container">
            <div className="row">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Technician</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                    <div className="form-floating mb-3">
                        <input required type="text" name="first_name" id="first_name" className="form-control"
                        value={formData.first_name} onChange={handleInputChange} />
                        <label htmlFor="first_name">First name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input required type="text" name="last_name" id="last_name" className="form-control"
                        value={formData.last_name} onChange={handleInputChange} />
                        <label htmlFor="last_name">Last name</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input required type="text" name="employee_id" id="employee_id" className="form-control"
                        value={formData.employee_id} onChange={handleInputChange} />
                        <label htmlFor="employee_id">Employee ID</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>
        </main>
    );
}
