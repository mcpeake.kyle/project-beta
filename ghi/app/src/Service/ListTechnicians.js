import React, {useEffect, useState } from 'react';

export default function TechnicianList(props) {
  const [technician, setTechnician] = useState([])

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/technicians/';
    const response = await fetch(url);
    if (response.ok) {
        const {technician} = await response.json();
        setTechnician(technician)
    }
}

    useEffect(() => {
      fetchData()
    }, [])

    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
          </tr>
        </thead>
        <tbody>
          {technician?.map((technician, index) => {
            return (
              <tr key={index}>
                <td>{ technician.id }</td>
                <td>{ technician.employee_id }</td>
                <td>{ technician.first_name }</td>
                <td>{ technician.last_name }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
