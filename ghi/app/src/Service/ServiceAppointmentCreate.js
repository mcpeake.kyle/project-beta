import React, { useState, useEffect } from 'react'

export default function CreateAppointment() {
    const [formData, setFormData] = useState({
        vin: '',
        vip: '',
        customer: '',
        date: '',
        time: '',
        technician: '',
        reason: '',
    })

    const handleInputChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        const autoUrl = 'http://localhost:8100/api/automobiles/'
        const autoResponse = await fetch(autoUrl)
        if (autoResponse.ok) {
            const autoData = await autoResponse.json()
            for (let auto of autoData.autos) {
                if (auto.vin == formData.vin) {
                    formData.vip = "Yes"
                } else {
                    formData.vip = "No"
                }
            }
        }

        const url = 'http://localhost:8080/api/appointments/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setFormData({
                vin: '',
                vip: '',
                customer: '',
                technician: '',
                date: '',
                time: '',
                reason: '',
            })
        }
    }

    const [technician, setTechnician] = useState([])

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setTechnician(data.technician)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <main>
            <div className="container">
            <div className="row">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Service Appointment</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                    <div className="form-floating mb-3">
                        <input required type="text" name="vin" id="vin" className="form-control"
                        value={formData.vin} onChange={handleInputChange} />
                        <label htmlFor="vin">Automobile VIN</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input required type="text" name="customer" id="customer" className="form-control"
                        value={formData.customer} onChange={handleInputChange} />
                        <label htmlFor="customer">Customer</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input type="date" name="date" id="date" className="form-control" value={formData.date} onChange={handleInputChange} />
                    </div>
                    <div className="form-floating mb-3">
                        <input type="time" name="time" id="time" className="form-control" value={formData.time} onChange={handleInputChange} />
                    </div>

                    <div className="mb-3">
                        <select required name="technician" id="technician" className="form-select" value={formData.technician} onChange={handleInputChange}>
                        <option value="">Choose a Technician</option>
                        {technician.map(technician => {
                            return (
                                <option key={technician.employee_id} value={technician.employee_id}>{technician.first_name} {technician.last_name}</option>
                            )
                        })}
                        </select>
                    </div>
                    <div className="form-floating mb-3">
                        <input required type="text" name="reason" id="reason" className="form-control"
                        value={formData.reason} onChange={handleInputChange} />
                        <label htmlFor="reason">Reason</label>
                    </div>
                    <button className="btn btn-primary">Create</button>

                    </form>
                </div>
                </div>
            </div>
        </main>
    );
}
