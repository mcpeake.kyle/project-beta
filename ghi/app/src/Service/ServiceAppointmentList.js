import React, {useEffect, useState } from 'react';

export default function ServiceAppointmentList(props) {
  const [appointment, setAppointment] = useState([])

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/appointments/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        const createdList = []
        for (let appt of data.Appointment) {
          if (appt.status == "created") {
            createdList.push(appt)
          }
        }
        setAppointment(createdList)
    }
  }

  const handleUpdate = async (event) => {
    const id = event.target.value
    const status = event.target.name
    const url = `http://localhost:8080/api/appointment/${id}/`
    const getResponse = await fetch(url)
    if (getResponse.ok) {
      const appt = await getResponse.json()
      appt.status = status
      let updateUrl
      if (status === "canceled") {
        updateUrl = `http://localhost:8080/api/appointments/${id}/cancel/`
      } else {
        updateUrl = `http://localhost:8080/api/appointments/${id}/finish/`
      }
      const fetchConfig = {
        method: 'put',
        body: JSON.stringify(appt),
        headers: {
          'Content-Type': 'application/json'
        }
      }
      const putResponse = await fetch(updateUrl, fetchConfig)
      if (putResponse.ok) {
        fetchData()
      }
    }
  }

    useEffect(() => {
      fetchData()
    }, [])

    return (
      <div className="shadow p-4 mt-4">
        <h1>Service Appointments</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>ID</th>
              <th>VIN</th>
              <th>VIP Status</th>
              <th>Customer</th>
              <th>Date</th>
              <th>Time</th>
              <th>Technician</th>
              <th>Reason</th>
            </tr>
          </thead>
          <tbody>
            {appointment?.map((appt, index) => {
              return (
                <tr key={(index)}>
                  <td>{ appt.id }</td>
                  <td>{ appt.vin }</td>
                  <td>{ appt.vip }</td>
                  <td>{ appt.customer }</td>
                  <td>{ appt.date }</td>
                  <td>{ appt.time }</td>
                  <td>{ appt.technician }</td>
                  <td>{ appt.reason }</td>
                  <td><button style={{color: "red"}} name={ "canceled" } value={ appt.id } onClick={handleUpdate}>Cancel</button></td>
                  <td><button style={{color: "green"}} name={ "finished" } value={ appt.id } onClick={handleUpdate}>Finish</button></td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
