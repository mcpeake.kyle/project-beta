import React, {useEffect, useState } from 'react';

export default function ServiceHistory(props) {
  const [appointmentData, setAppointmentData] = useState([])
  const [appt, setAppt] = useState([])
  const [searchInput, setSearchInput] = useState("")

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/appointments/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        setAppointmentData(data.Appointment)
        setAppt(data.Appointment)
    }
  }

  const handleChange = (event) => {
    setSearchInput(event.target.value)
  }
  const handleSubmit = (event) => {
    event.preventDefault()
    let matches = []
    for (let appt of appointmentData) {
      if (appt.vin === searchInput) {
        matches.push(appt)
      }
    }
    setAppt(matches)
  }

  useEffect(() => {
    fetchData()
  }, [])

  return (
    <div className="shadow p-4 mt-4">
      <h1>Service History</h1>
      <form onSubmit={handleSubmit}>
        <div className="form-floating mb-3">
          <input type="text" name="searchInput" id="searchInput" className="form-control"
          value={searchInput} onChange={handleChange} />
          <label htmlFor="searchInput">Search by VIN</label>
          <button>Submit</button>
        </div>
      </form>
      <div>
        <button onClick={fetchData}>Display All</button>
      </div>

      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>VIP Status</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {appt.map((appt, index) => {
            return (
              <tr key={index}>
                <td>{ appt.vin }</td>
                <td>{ appt.vip }</td>
                <td>{ appt.customer }</td>
                <td>{ appt.date }</td>
                <td>{ appt.time }</td>
                <td>{ appt.technician }</td>
                <td>{ appt.reason }</td>
                <td>{ appt.status }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
  </div>
  );
}
