from django.db import models

class AutomobileVo(models.Model):
    vin = models.CharField(max_length=200, unique=True)
    sold = models.BooleanField()

class Salesperson(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200, unique=True)

class Customer(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=10)
    address = models.TextField()

class Sale(models.Model):
    price = models.PositiveIntegerField()
    automobile = models.ForeignKey(
        AutomobileVo,
        related_name='sale',
        on_delete=models.CASCADE,
    )
    salesperson = models.ForeignKey(
        Salesperson,
        related_name='sales',
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name='sales',
        on_delete=models.CASCADE,
    )
