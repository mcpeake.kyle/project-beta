from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import AutomobileVo, Salesperson, Customer, Sale
import json
from common.json import ModelEncoder
from django.http import JsonResponse


class AutomobileVoEncoder(ModelEncoder):
    model = AutomobileVo
    properties = [
        'vin',
        'sold',
    ]

class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        'first_name',
        'last_name',
        'employee_id',
    ] 

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        'first_name',
        'last_name',
        'phone_number',
        'address',
        'id'
    ]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        'automobile',
        'salesperson',
        'customer',
        'price',
        'id'
    ]
    encoders = {
        'automobile': AutomobileVoEncoder(),
        'salesperson': SalespersonEncoder(),
        'customer': CustomerEncoder(),
    }

@require_http_methods(["GET", "POST" ])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {'salespeople': salespeople},
            encoder=SalespersonEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse (
                {"message": "Salesperson Does Not Exist"},
                status=400,
            )
@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_salesperson(request, id):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(id=id)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {'messagage': "Salesperson Does Not Exist"},
                status=400,
            )
    elif request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(id=id)
            salesperson.delete()
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {'message': "Saleperson Does Not Exist"}
            )
    else:
        content = json.loads(request.body)
        Salesperson.filter(id=id).update(**content)
        salesperson = Salesperson.get(id=id)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )
@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            customers = Customer.objects.create(**content)
            return JsonResponse(
                customers,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {'message': "Customer Does Not Exist"},
                status=400,
            )
@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_customers(request, id):
    if request.method == "GET":
        try:
            customers = Customer.objects.get(id=id)
            return JsonResponse(
                customers,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer Does Not Exist"},
                status=400,
            )
    elif request.method == "DELETE":
        try:
            customers = Customer.objects.get(id=id)
            customers.delete()
            return JsonResponse(
                customers,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer Does Not Exist"}
            )
    else:
        content = json.loads(request.body)
        Customer.filter(id=id).update(**content)
        customers = Customers.get(id=id)
        return JsonResponse(
            customers,
            encoder=CustomerEncoder,
            safe=False,
        )
@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            automobile = AutomobileVo.objects.get(vin=content["automobile"])
            content["automobile"] = automobile

            salesperson = Salesperson.objects.get(id=content['salesperson'])
            content['salesperson'] = salesperson

            customer = Customer.objects.get(id=content['customers'])
            content['customer'] = customer
            sale = Sale.objects.create(**content)
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except (Sale.DoesNotExist, AutomobileVo.DoesNotExist, Salesperson.DoesNotExist, Customer.DoesNotExist):
            return JsonResponse(
                {"message": "Sale Does Not Exist"},
                status=400,
            )
@require_http_methods(["GET", "DELETE"])
def api_show_sale(request, id):
    if request.method == "GET":
        try:
            sales = Sale.objects.get(id=id)
            return JsonResponse(
                sales,
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Sales Does Not Exist"},
                status=400,
            )
    else: 
        request.method == "DELETE"
        try:
            sales = Sale.objects.get(id=id)
            sales.delete()
            return JsonResponse(
                sales,
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {'message': "Sales Does Not Exist"}
            )
