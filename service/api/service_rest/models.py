from django.db import models


# Create your models here.
class Technician(models.Model):
    id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.CharField(max_length=50, unique=True)


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


class Appointment(models.Model):
    id = models.AutoField(primary_key=True)
    date = models.CharField(max_length=100)
    time = models.CharField(max_length=100)
    reason = models.CharField(max_length=100)
    status = models.CharField(max_length=20, default="created")
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=100)
    technician = models.CharField(max_length=50)
    vip = models.CharField(max_length=10)
