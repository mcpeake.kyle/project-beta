from django.urls import path
from .views import list_technicians, specific_technician, appointments, specific_appointment, cancel_appointment, finish_appointment


urlpatterns = [
     path('technicians/',
          list_technicians,
          name="list_technicians"),

     path('technician/<int:id>/',
          specific_technician,
          name="specific_technician"),

     path('appointments/',
          appointments,
          name="appointments"),

     path('appointment/<int:id>/',
          specific_appointment,
          name="specific_appointment"),

     path('appointments/<int:id>/cancel/',
          cancel_appointment,
          name="cancel_appointment"),

     path('appointments/<int:id>/finish/',
          finish_appointment,
          name="finish_appointment")
]
