from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from common.json import ModelEncoder
from .models import Technician, AutomobileVO, Appointment


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "vin",
        "sold",
    ]


class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        'id',
        "first_name",
        "last_name",
        "employee_id",
    ]


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        'id',
        "date",
        "time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
        "vip",
    ]


@require_http_methods(["GET", "POST"])
def list_technicians(request, id=None):
    if request.method == "GET":
        if id is not None:
            tech = Technician.objects.filter(id=id)
        else:
            tech = Technician.objects.all()
        return JsonResponse(
            {"technician": tech},
            encoder=TechnicianDetailEncoder,
        )
    else:
        content = json.loads(request.body)
        tech = Technician.objects.create(**content)
        return JsonResponse(
            tech,
            encoder=TechnicianDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def specific_technician(request, id):
    if request.method == "GET":
        tech = Technician.objects.get(pk=id)
        return JsonResponse(
            tech,
            encoder=TechnicianDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(pk=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        # PUT / UPDATE
        content = json.loads(request.body)
        Technician.objects.filter(pk=id).update(**content)
        tech = Technician.objects.get(pk=id)
        return JsonResponse(
            tech,
            encoder=TechnicianDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def appointments(request, vin=None):
    if request.method == "GET":
        if vin is not None:
            appt = Appointment.objects.filter(pk=vin)
        else:
            appt = Appointment.objects.all()
        return JsonResponse(
            {"Appointment": appt},
            encoder=AppointmentDetailEncoder,
        )
    else:
        content = json.loads(request.body)
        appt = Appointment.objects.create(**content)
        return JsonResponse(
            appt,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def specific_appointment(request, id):
    if request.method == "GET":
        appt = Appointment.objects.get(pk=id)
        return JsonResponse(
            appt,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(pk=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Appointment.objects.filter(pk=id).update(**content)
        appt = Appointment.objects.get(pk=id)
        return JsonResponse(
            appt,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )


@require_http_methods(["PUT"])
def cancel_appointment(request, id):
    content = json.loads(request.body)
    Appointment.objects.filter(id=id).update(**content)
    appt = Appointment.objects.get(id=id)
    return JsonResponse(
        appt,
        encoder=AppointmentDetailEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def finish_appointment(request, id):
    content = json.loads(request.body)
    Appointment.objects.filter(id=id).update(**content)
    appt = Appointment.objects.get(id=id)
    return JsonResponse(
        appt,
        encoder=AppointmentDetailEncoder,
        safe=False,
    )
