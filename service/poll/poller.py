import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

# Import models from service_rest, here. Ignore vs-code error hinting
# from service_rest.models import Something
from service_rest.models import AutomobileVO


def getAutos():
    response = requests.get("http://inventory-api:8000/api/automobiles/")
    # print("Poller response is:", response)
    content = json.loads(response.content)
    for autos in content['autos']:
        # print("print statement is", autos)
        AutomobileVO.objects.update_or_create(
            import_href=autos['href'],
            defaults={
                "vin": autos["vin"],
                "sold": autos["sold"],
            }
        )


def poll():
    while True:
        print('Service poller polling for Automobile data')
        try:
            getAutos()
        except Exception as e:
            print(e, file=sys.stderr)

        time.sleep(60)


if __name__ == "__main__":
    poll()
